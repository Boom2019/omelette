package fr.eql.ad1.omelette;

public class OmeletteFactory {

    public OmeletteFactory() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Effectue une omelette savoureuse
     * @param nbOeufs nombre d'oeufs a casser
     */
    public void createOmelette(int nbOeufs)
    {
        for (int i = 0; i < nbOeufs; i++) {
            Oeuf oeuf = new Oeuf();
            oeuf.casser();
        }
    }
    
}
